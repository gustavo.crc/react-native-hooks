import { SafeAreaView, StyleSheet, View } from 'react-native';
import Home from './src/Telas/Home';

export default function App() {
  return (
    <SafeAreaView style={estilos.tela}>
      <Home />
    </SafeAreaView>
  );
}

const estilos = StyleSheet.create({
  tela: {
    flex: 1
  }
})