import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet, Text } from "react-native";

import Produtor from "./Produtor";
import useProdutores from "../../../hooks/useProdutores";

export default function Produtores({ topo: Topo }) {
  // SEMPRE CHAMAR O HOOKS NO COMEÇO DA FUNÇÃO!
  const [titulo, produtores] = useProdutores()

  const TopoLista = () => {
    return (
      <>
        <Topo />
        <Text style={estilos.titulo}>{titulo}</Text>
      </>
    )
  }

  return (
    <FlatList
      data={produtores}
      renderItem={({ item }) => <Produtor {...item} />}
      keyExtractor={({ nome }) => nome}
      ListHeaderComponent={TopoLista}
    />
  )
}

const estilos = StyleSheet.create({
  titulo: {
    fontSize: 20,
    lineHeight: 32,
    marginHorizontal: 16,
    marginTop: 16,
    fontWeight: "bold",
    color: "#464646"
  }
})