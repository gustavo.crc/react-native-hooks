import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

import { carregaTopo } from "../../../servicos/carregarDados";

// Utilizando o exemplo de componente de class no React Native
class Topo extends React.Component {
  // Estado default
  state = {
    topo: {
      boasVindas: '',
      legenda: '',
      logo: ''
    }
  }

  atualizaTopo() {
    const retorno = carregaTopo()
    this.setState({ topo: retorno })
  }

  // é executado quando o componente termina de renderizar
  componentDidMount(){
    this.atualizaTopo()
  }

  render() {
    return (
      <View style={estilos.topo}>
        <Image source={this.state.topo.logo} style={estilos.imagem} />
        <Text style={estilos.titulo}>{this.state.topo.boasVindas}</Text>
        <Text style={estilos.legenda}>{this.state.topo.legenda}</Text>
      </View>
    )
  }
}

const estilos = StyleSheet.create({
  topo: {
    backgroundColor: '#F6F6F6',
    padding: 16
  },
  imagem: {
    width: 70,
    height: 28
  },
  titulo: {
    marginTop: 24,
    fontSize: 26,
    lineHeight: 42,
    fontWeight: 'bold',
    color: "#464646"
  },
  legenda: {
    fontSize: 16,
    lineHeight: 26,
    color: "#A3A3A3"
  }
})

export default Topo