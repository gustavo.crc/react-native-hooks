import { useState, useEffect } from "react";

import { carregaProdutores } from "../servicos/carregarDados";

export default function useProdutores() {
  // titulo está representando o estado 
  // setTitulo é o método que irá alterar o estado
  // useState('') seria o valor inicial do componente
  const [titulo, setTitulo] = useState('')
  const [produtores, setProdutores] = useState([])

  // Quando passamos array vazio no segundo parametro é pra que execute 1 vez quando a nossa tela estiver carregada,
  // o que está dentro da função irá executar assim que o componente for carregado.
  useEffect(() => {
    const retorno = carregaProdutores()
    retorno.produtores.sort(
      (produtor1, produtor2) => produtor1.distancia - produtor2.distancia,
    )
    setTitulo(retorno.titulo)
    setProdutores(retorno.produtores)
  }, [])

  return (
    [titulo, produtores]
  )
}