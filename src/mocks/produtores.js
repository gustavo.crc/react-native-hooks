import green from "../../assets/produtores/green.png"
import salad from "../../assets/produtores/salad.png"
import jenny from "../../assets/produtores/jenny-jack.png"
import grow from "../../assets/produtores/grow.png"
import potager from "../../assets/produtores/potager.png"

const gerarNumRandom = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

const produtores = {
  titulo: 'Produtores',
  produtores: [
    {
      nome: 'Green Farm',
      imagem: green,
      distancia: gerarNumRandom(1, 900),
      estrelas: `${gerarNumRandom(1, 5)}`
    },
    {
      nome: 'Salad Farm',
      imagem: salad,
      distancia: gerarNumRandom(1, 900),
      estrelas: `${gerarNumRandom(1, 5)}`
    },
    {
      nome: 'Jenny Jack Farm',
      imagem: jenny,
      distancia: gerarNumRandom(1, 900),
      estrelas: `${gerarNumRandom(1, 5)}`
    },
    {
      nome: 'Grow Farm',
      imagem: grow,
      distancia: gerarNumRandom(1, 900),
      estrelas: `${gerarNumRandom(1, 5)}`
    },
    {
      nome: 'Potager de la Rocade',
      imagem: potager,
      distancia: gerarNumRandom(1, 900),
      estrelas: `${gerarNumRandom(1, 5)}`
    }
  ]
}

export default produtores