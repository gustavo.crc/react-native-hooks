import logo from "../../assets/logo.png"

const topo = {
  logo: logo,
  boasVindas: 'Olá, Gustavo',
  legenda: 'Encontre os melhores produtores'
}

export default topo